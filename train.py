import sys
import argparse
import pandas as pd
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
from nn_arch import LSTM_network
from matplotlib import pyplot as plt
import pickle

sys.path.append('..')

class Parser():
    def __init__(self):
        parser = argparse.ArgumentParser()
        parser.add_argument("-wmax", "--words-max", required=False, default=4944, help="Max number of words")
        parser.add_argument("-smax", "--sequence-max", required=False, default=35, help="Max number of words in input")
        parser.add_argument("-edim", "--embedding-dim", required=False, default=2, help="Embedding dimension")
        parser.add_argument("-e", "--epoch", required=True, help="Number of epochs")
        parser.add_argument("-b", "--batch", required=True, help="Batch size")
        parser.add_argument("-s", "--save", required=False, default="trainedModel", help="save model")
        args = vars(parser.parse_args())

        self.WORDS_MAX = int(args['words_max'])
        self.SEQUENCE_MAX = int(args['sequence_max'])
        self.EMBEDDING_DIM = int(args['embedding_dim'])
        self.EPOCHS = int(args['epoch'])
        self.BATCH = int(args['batch'])
        self.MODEL_BASENAME = args['save']


if __name__ == "__main__":
    # Load arguments
    args = Parser()

    # Load and Normalize data (Spam: 747, Ham: 900)
    data = pd.read_csv('datasets/spam.csv')
    spam = data[data['Category'] == 'spam']
    ham = data[data['Category'] == 'ham']
    data = spam.append(ham[0:900])

    messages = data['Message']
    category = data['Category']


    # Text Preprocessing
    tokenizer = Tokenizer(num_words=args.WORDS_MAX, \
                          filters='!"#$%&()*+,-./:;<=>?@[\]^_`{|}~', lower=True)
    tokenizer.fit_on_texts(messages.values)
    word_index = tokenizer.word_index
    print('Found %s unique tokens.' % len(word_index))
    print('Found longest sequence: %s ' % len(max(messages)))

    # Load tranning data
    X = tokenizer.texts_to_sequences(messages.values)
    X = pad_sequences(X, maxlen=args.SEQUENCE_MAX)
    Y = pd.get_dummies(category).values

    # Train test split
    X_train, X_test, Y_train, Y_test = train_test_split(X,Y, test_size=0.10)
    print('Shape of train X: {}, Y {:}'.format(X_train.shape, Y_train.shape))
    print('Shape of test X: {}, Y {:}'.format(X_test.shape, Y_test.shape))

    # Load model
    model = LSTM_network.build2(args.WORDS_MAX, args.EMBEDDING_DIM, X)
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    history = model.fit(X_train, Y_train, epochs=args.EPOCHS, batch_size=args.BATCH, validation_split=0.25)
    print(history.history.keys())

    # Evaluation
    accuracy = model.evaluate(X_test, Y_test)
    print('Test set\n  Loss: {:0.3f}\n  Accuracy: {:0.3f}'.format(accuracy[0], accuracy[1]))

    # Plot
    plt.title('Loss')
    plt.plot(history.history['loss'], label='train')
    plt.plot(history.history['val_loss'], label='test')
    plt.legend()
    plt.show()

    plt.title('Accuracy')
    plt.plot(history.history['accuracy'], label='train')
    plt.plot(history.history['val_accuracy'], label='test')
    plt.legend()
    plt.show()

    # Save model to disk
    model.save(args.MODEL_BASENAME + ".h5")

    # Save tokenizer
    with open(args.MODEL_BASENAME + '.pkl', 'wb') as output:
        pickle.dump(tokenizer, output, pickle.HIGHEST_PROTOCOL)
