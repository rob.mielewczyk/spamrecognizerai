from flask import Flask, render_template, request
import numpy as np
import pickle
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.sequence import pad_sequences
from ..predict import load_model_and_tokenizer
app = Flask(__name__)

@app.route('/')
def index():
    return render_template("index.html")

@app.route('/response', methods=['POST'])
def make_prediction():
    text = request.form.get("text")
    ai_message, probability = ai_evaluate_text(text)
    return render_template("index.html", ai_message=ai_message, probability=probability)

def ai_evaluate_text(text):
    MODEL_FOLDER = "saved_models"
    MODEL_NAME = "trainedModel"
    model, tokenizer = load_model_and_tokenizer("{}/{}".format(MODEL_FOLDER, MODEL_NAME))

     # My data
    spam_message = []
    spam_message.append(text)
    spam_message = tokenizer.texts_to_sequences(spam_message)

    # My tests
    padded = pad_sequences(spam_message, maxlen=model.input_shape[1])
    pred = model.predict(padded)
    labels = ['ham', 'spam']
    ai_message = "{}".format(labels[np.argmax(pred)])
    probability = "{}".format(pred)

    return ai_message, probability

