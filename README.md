# About Project
Spam messages recognition AI. When trained achives 94.05% of accuracy on test data correctly recognizing and labelling messages that are spam and not spam

## Dataset
747 - spam messages
1. "URGENT! We are trying to contact you are a winner"
2. "You are a winner U have been specially selected 2 receive £1000 cash…"<br>

4825 - ham messages (the ones not labeled as spam)
1. "New car and house for my parents.:)i have only new job in hand:)"
2. "I place all ur points on e cultures module already."


# How to run
### Install requirements
```python
python=3.6 (because of dependency on tensorflow)
pip install -r requirements.txt
```
### Train model on dataset
```python
python train.py 
    [--words-max] : (amount of unique words in memory)
    [--sequence-max] : (max words in one sequence)
    [--embedding-dim] : (embedding-dimensions)
    --epoch : (number of epochs)
    --batch : (batch size)
    [--save] : (save trained model)    
```

### Training example
```python
Python3 train.py --epoch 40 --batch 64 [-wmax -smax -edim --save]
```

### Predict your own text with
```python
python predict.py --model-name trained_model --phrase "Urgent u won an iphone come an get it"
```

# Attach this AI to your website
## configure flask with windows
```powershell
$env:FLASK_APP = "app.py"
python -m flask run
```
## configure with linux
```bash
export FLASK_APP=app.py
python -m flask run
```
## example website
![spam](./readme_images/spam_immage.PNG)
![ham](./readme_images/spam_safe.PNG)






