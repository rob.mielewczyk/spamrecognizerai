from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Embedding
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import SpatialDropout1D

class LSTM_network:
    def build(max_words, embedding_dimensions, X):
        # Network architecture:
        # INPUT => EMBEDDING => DROPOUT => LSTM => DROPOUT => DENSE => SOFTMAX
        model = Sequential()

        model.add(Embedding(max_words, embedding_dimensions, input_length=X.shape[1]))
        model.add(SpatialDropout1D(0.2))
        model.add(LSTM(100, dropout=0.2, recurrent_dropout=0.2))
        model.add(Dense(2, activation='softmax'))

        return model

    def build2(max_words, embedding_dimensions, X):
        # Network architecture:
        # INPUT => EMBEDDING => LSTM => DROPOUT => LSTM => DROPOUT => DENSE => SOFTMAX
        model = Sequential()

        model.add(Embedding(max_words, embedding_dimensions, input_length=X.shape[1]))
        model.add(LSTM(128, return_sequences=True))
        model.add(Dropout(0.5))
        model.add(LSTM(128))
        model.add(Dropout(0.5))
        model.add(Dense(2, activation='softmax'))
        model.add(Activation('softmax'))

        return model

    def build_convLSTM(max_words, embedding_dimensions, X):
        pass