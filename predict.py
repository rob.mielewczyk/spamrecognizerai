import numpy as np
import pickle
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.sequence import pad_sequences

def load_model_and_tokenizer(MODEL_NAME):
    model = load_model(MODEL_NAME + ".h5")
    with open(MODEL_NAME + '.pkl', 'rb') as input:
        tokenizer = pickle.load(input)
    return model, tokenizer

def load_arguments():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--model-name", required=False, default="trainedModel", help="name of your model dollowed by .h5")
    parser.add_argument("-p", "--phrase", required=False, default="this is not spam", help="Phrase you want to give to AI")
    args = vars(parser.parse_args())

    return args["model_name"], args["phrase"]


if __name__ == '__main__':
    model_name, phrase = load_arguments()
    model, tokenizer = load_model_and_tokenizer(model_name)

    # My data
    spam_message = []
    spam_message.append(phrase)
    spam_message = tokenizer.texts_to_sequences(spam_message)

    # My tests
    padded = pad_sequences(spam_message, maxlen=model.input_shape[1])
    pred = model.predict(padded)
    labels = ['ham', 'spam']
    print(pred, labels[np.argmax(pred)])
